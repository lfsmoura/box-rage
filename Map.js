/************************************
* Tile
*************************************/
function Tile(color, solid) {
  this.color = color;
  this.solid = solid;
}

Tile.prototype.draw = function(x, y) {
  if(!this.solid)
    return;
  ctx.fillStyle = this.color;
	ctx.fillRect(x, y, tileSize, tileSize);
}

/************************************
* Map
*************************************/
function Map(map, objects) {
  for(var i = 0; i < map.length; i++)
    map[i] = map[i].concat(map[i]);

  this.map = map.slice(0);
  this.offsetX = 0;
  this.offsetY = 0;
  this.objects = objects.slice(0);
}

Map.prototype.objectsInRangeX = function(x0, x1) {
  return this.objects.filter( function(obj) {
    return !((obj.pos.x + obj.width < x0) || (obj.pos.x > x1)); 
  });
} 

Map.prototype.objectsInRangeXY = function(x0, x1, y0, y1) {
  return this.objectsInRangeX(x0,x1).filter( function(obj) {
    return !((obj.pos.y > y1) || (obj.pos.y + obj.height < y0)); 
  });
}

Map.prototype.removeObject = function(elem) {
  this.objects.splice(this.objects.indexOf(elem),1);
}

Map.prototype.draw = function(hero) {

  with(this) {
    ctx.fillStyle = "rgb(200, 200, 240)";
    ctx.fillRect(0, 0, tilesOnScreenHorizontal * tileSize, tilesOnScreenVertical * tileSize);

  
    offsetY = 0; //Math.min(0, x / tileSize);
    offsetX = Math.max(0, hero.pos.x - 3 * tileSize);
    // TODO: draw only til the screen limits
    // TODO: draw the hero-facing part of the map
    for(y = 0; y <= tilesOnScreenVertical; y++) {
      for(x = 0; x <= tilesOnScreenHorizontal; x++) {
        tiles[this.map[y][x + Math.floor(offsetX/tileSize)]].draw(x * tileSize - (offsetX % tileSize) , y * tileSize);
      }
    }
    
    var items = objectsInRangeX(offsetX - tileSize, offsetX + (tilesOnScreenVertical + 5) * tileSize); 
    items.forEach(function(obj) { obj.draw(offsetX, offsetY); });
  }
}

Map.prototype.objectsOn = function(x, y) {
  return this.objectsInRangeXY(x-1,x+1, y-1, y+1);
}

Map.prototype.tileOn = function(x, y) {
  var i = Math.max(0, Math.floor(y / tileSize)),
      j = Math.max(0, Math.floor(x / tileSize));

  return tiles[this.map[i][j]]; 
};

Map.prototype.tilesOnLine = function(x0, y0, x1, y1) {
  x0 = Math.max(0, x0);
  y0 = Math.max(0, y0);
  x1 = Math.max(0, x1);
  y1 = Math.max(0, y1);
  var i0 = Math.floor((y0) / tileSize),
      i1 =Math.floor((y1) / tileSize),
      j0 = Math.floor((x0) / tileSize),
      j1 = Math.floor((x1) / tileSize);
  var solidTiles = [];
  for(var i = i0; i <= i1; i++)
    for(var j = j0; j <= j1; j++)
      if(tiles[this.map[i][j]].solid){
        solidTiles.push(tiles[this.map[i][j]]);
      }
  return solidTiles;
};

Map.prototype.update = function() {
  var map = this
  this.objects.forEach(function(elem) { 
    elem.update(map);
  });
};
