/************************************
* Game Setup
* TODO: load game variables from file
*************************************/
var tilesOnScreenHorizontal = 22,
    tilesOnScreenVertical   = 17;
var boardOffsetX = 0;
var boardOffsetY = 0;
var tileSize = 30;
var ctx;
var frameRate = 60;
var timeBetweenFrames = (1000 / frameRate);
var G = 1.5; //timeBetweenFrames * 1000 * 10;
var Epsilon = 0.00000001;
var pressedKey = Array(256);
var imap = [ [1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1],
            [1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1],
            [1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1],
            [1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1],
            [1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1],
            [1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1],
            [1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1],
            [1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1,1,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1],
            [1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1],
            [1,0,0,0,0,0,0,0,0,0,0,1,1,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1],
            [1,0,0,0,0,0,0,0,0,0,0,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1],
            [1,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1],
            [1,1,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1],
            [1,1,1,1,0,0,0,0,0,0,0,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1],
            [1,1,1,1,0,0,0,0,0,0,0,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1],
            [1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,0,0,1,1,1,1,1,1,1,1,1,1,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1],
            [1,1,1,1,1,1,1,1,1,1,1,0,0,0,1,1,1,1,1,1,1,1,1,1,0,0,0,1,1,1,1,1,1,1,1,1,1,0,0,1,1,1,1,1,1,1,1,1,1,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1],
            [1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1]];

var tiles = new Array();
tiles[0] = new Tile("rgb(255,255,255)", false);
tiles[1] = new Tile("rgb(0,200,0)", true);
tiles[2] = new Tile("rgb(0,100,0)", true);

var box = new Box(300, 10);
var hero = new Hero(100,300);

var objects = new Array();
objects.push(box);
objects.push(new Coin(5*tileSize, 14*tileSize));
objects.push(new Coin(7*tileSize, 13*tileSize));
objects.push(new Coin(15*tileSize, 13*tileSize));
objects.push(new Coin(16*tileSize, 4*tileSize));
objects.push(new Coin(18*tileSize, 9*tileSize));

objects.push(new Enemy(6*tileSize, 10*tileSize));
objects.push(new Enemy(19*tileSize, 10*tileSize));
objects.push(hero);
var currMap = new Map(imap, objects);

var fcount = 0;

/* */

mockCoin = new Coin(5,30);
function drawCoins(nCoins) {
  mockCoin.draw(0,0);
  drawMessage("x " + nCoins, 40, 52, "10pt Arial");
}

function drawHp(hp) {
  ctx.fillStyle = "rgb(255,0,0)";
	ctx.fillRect(10, 10, 191, 20); 
	ctx.fillStyle = "rgb(255,255,0)";
	for(var i = 0; i < Math.floor(hp/10); i++) {
	  ctx.fillRect(11 + i*19, 11, 18, 18); 
	}
}

function init() {
  output = document.getElementById("output");
  canvas = document.getElementById("canvas");
  canvas.width = tilesOnScreenHorizontal * tileSize;
  canvas.height = tilesOnScreenVertical * tileSize;
  
	ctx = canvas.getContext('2d');
  
  document.onmousedown = mouseDown;
  document.onmousemove = mouseMove;
  document.onmouseup = mouseUp;
  document.onkeydown = keyCheck;
  document.onkeyup = keyRelease;
  mainLoop();
}

show = function(num) { return Math.floor(num * 10)/ 10; }

mainLoop = function() {
  output.innerHTML = "box.acc= (" + show(box.speed.x) + ", " + show(box.speed.y) + ") <br> hero.pos= (" + show(hero.pos.x) + ", " + show(hero.pos.y) + ") <br> hero.speed = (" + show(hero.speed.x) + ", " + show(hero.speed.y) + ") " + show(currMap.offsetX) + "   " + (fcount++);
  
  currMap.update();
  currMap.draw(hero);
  
  drawCoins(hero.coins);
  drawHp(hero.hp);
    
  setTimeout("mainLoop()", timeBetweenFrames);
}

function keyRelease(e) {
  pressedKey[e.keyCode] = false;
}

function keyCheck(e) {
  pressedKey[e.keyCode] = true;

}

function mouseUp(e) {
  box.releaseTarget();
}

function mouseMove(e) {
  if(box.followingMouse)
    box.setTarget(e.pageX - canvas.offsetLeft, e.pageY - canvas.offsetTop);
}

function mouseDown(e) {
  box.setTarget(e.pageX - canvas.offsetLeft, e.pageY - canvas.offsetTop);
}
