/************************************
* Vector
*************************************/
function Vector(x,y) {  
  this.x = x;
  this.y = y;
}

Vector.prototype.set = function(x,y) {
  this.x = x;
  this.y = y;
}

Vector.prototype.addVector = function(vector) {
  this.x += vector.x;
  this.y += vector.y;
}

Vector.prototype.subtractVector = function(vector) {
  this.x -= vector.x;
  this.y -= vector.y;
}

Vector.prototype.dot = function(vector) {
  this.x *= vector.x;
  this.y *= vector.y;
}

Vector.prototype.scalar = function(a) {
  this.x *= a;
  this.y *= a;
}

Vector.prototype.norm = function() {
  return Math.sqrt(this.x*this.x + this.y*this.y);
}

Vector.prototype.normalize = function() {
  var norm = this.norm();
  if(norm != 0) {
    this.x /= norm;
    this.y /= norm;
  }
}

subtractVectors = function(vec1, vec2) {
  vec3 = new Vector(vec1.x, vec1.y);
  vec3.subtractVector(vec2);
  return vec3;
}

scalarProduct = function(vector, a) {
  vector.x *= a;
  vector.y *= a;
  return vector;
}

/************************************
* Other Functions
*************************************/
function drawMessage(msg, x, y, font)
{
	ctx.fillStyle = "rgb(0,0,0)";  
	ctx.font = font;
	ctx.textAlign = "center";
	ctx.fillText(msg, x, y);
}

function lastDivisor(x, d) {
  return Math.floor(x/d)*d;  
}

function nextDivisor(x, d) {
  return (Math.floor(x/d)+1)*d;  
}

/************************************
* Constants
*************************************/
var Key = { 
  ENTER: 13,
  ESCAPE: 27,
  LEFT_ARROW: 37,
  UP_ARROW: 38,
  RIGHT_ARROW: 39,
  DOWN_ARROW: 40
}
