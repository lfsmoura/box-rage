var Animation = {
  MovingRight: 0,
  MovingLeft: 1,
  JumpingRight: 0,
  JumpingLeft: 1
}

/************************************
* Actor
*************************************/
Actor = function() {
  this.pos = new Vector(0, 0);
  this.speed = new Vector(0, 0);
  this.width = 30; 
  this.height = 30;
  this.onAir = true;
  this.collisionPoints = new Array();
  
  this.enableGravity = function() {
    return true;
  };
  
  this.sheet = new Image();
  this.sheet.src = "img/hero.png";
  this.sheetTileSize = 30;
  this.sheetOffsetX = 0;
  this.sheetOffsetY = 0;
  this.animOn = false;
  this.animIndex = 0;
  this.animPosition = 1;
  this.animTimeToAnim = 6;
  this.animCounter = 0;
}

Actor.prototype.onTouch = function(actor, map) { };

Actor.prototype.updatePosition = function(map) {
  // TODO: refactor
  var actor = this;
  map.objectsInRangeXY(this.pos.x, this.pos.x + this.width, this.pos.y, this.pos.y + this.height).forEach(function(elem) {
    // TODO: pass spot
    if(elem !== actor)
      elem.onTouch(actor, map);
  });

  // horizontal
  // left
  var leftTiles = map.tilesOnLine(this.pos.x + this.speed.x, this.pos.y+1, this.pos.x + this.speed.x, this.pos.y + this.height-1),
      rightTiles = map.tilesOnLine(this.pos.x + this.width + this.speed.x, this.pos.y+1, this.pos.x + this.width + this.speed.x, this.pos.y + this.height-1);

  // horizontal
  var colLeft = leftTiles.length > 0,
      colRight = rightTiles.length > 0;
  if(this.speed.x < -Epsilon && colLeft) {
    this.pos.x -= this.pos.x % tileSize;
    this.speed.x = 0;
  } else if (this.speed.x > Epsilon && colRight) {
    this.pos.x = lastDivisor(this.pos.x + this.width + this.speed.x, tileSize) - this.width;
    this.speed.x = 0;
  }
    
  this.pos.x += Math.round(this.speed.x);

  // vertical
  // bottom
  var bottomTiles = map.tilesOnLine(this.pos.x+1, this.pos.y + this.height + this.speed.y, this.pos.x + this.width-1, this.pos.y + this.height + this.speed.y);

  var colBottom = bottomTiles.length > 0;
  if(!colBottom) {
    if(this.enableGravity()) {
      this.onAir = true;
      this.speed.y = Math.min(this.speed.y + G, 20);
    }
  } else if( this.speed.y > Epsilon) {
    this.onAir = false;
    this.pos.y = lastDivisor(this.pos.y + this.height + this.speed.y, tileSize) - this.height;
    this.speed.y = 0;
  } 
  var topTiles = map.tilesOnLine(this.pos.x+1, this.pos.y + this.speed.y, this.pos.x + this.width-1, this.pos.y + this.speed.y);
  var colTop = topTiles.length > 0;
  if(this.speed.y < -Epsilon && colTop){
    this.pos.y -= this.pos.y % tileSize;
    this.speed.y = 0;
  } 
  
  this.pos.y += Math.round(this.speed.y);

  if(-Epsilon < this.speed.x  && this.speed.x < Epsilon)
    this.speed.x = 0.0
  else if(!this.onAir)
    this.speed.x /= 1.1;
};

Actor.prototype.stopAnimation = function() {
  this.animOn = true;
  this.animCounter = 0;
  this.animPosition = 1;
};

// if animation is the same as current one, nothing changes
Actor.prototype.changeAnimation = function(index) {
  this.animOn = true;
  this.animIndex = index;
};

Actor.prototype.draw = function(offsetX, offsetY) {
  if(this.animOn){ 
    this.animCounter++;
    if(this.animCounter > this.animTimeToAnim) {
      this.animCounter = 0;
      this.animPosition = (this.animPosition + 1) % 4;
    }
  }

  ctx.drawImage(this.sheet, 
    this.animPosition*30, this.animIndex*this.sheetTileSize,
    this.sheetTileSize, this.sheetTileSize, 
    this.pos.x - offsetX - this.sheetOffsetX, this.pos.y - offsetY - this.sheetOffsetY,
    this.sheetTileSize, this.sheetTileSize);
};

/************************************
* Game Objects
*************************************/
function Coin(x, y) {
  this.pos = new Vector(x, y);
  this.width = tileSize;
  this.height = tileSize;
  
  this.onTouch = function(actor, map) {
    if(actor instanceof Hero) {
      map.removeObject(this);
      hero.coins += 1;
    }
  }
  
  this.sheet = new Image();
  this.sheet.src = "img/apple.png";
  this.animPosition = 0;
};

Coin.prototype = new Actor();

Coin.prototype.update = function(map) {};


/************************************
* Action
*************************************/
var Action = {
  WanderLeft: 1,
  WanderRight: 2
};

/************************************
* Enemy
*************************************/
function Enemy(x, y) {
  this.pos = new Vector(x, y);
  this.width = tileSize;
  this.height = tileSize;
  this.action = Action.WanderLeft;
  
  this.update = function(map) {
    switch(this.action) {
    case Action.WanderLeft:
      this.speed.x = -2;
      var leftTiles = map.tilesOnLine(this.pos.x + this.speed.x, this.pos.y+1, this.pos.x + this.speed.x, this.pos.y + this.height-1);
        
      if(leftTiles.length > 0)
        this.action = Action.WanderRight;
    break;
    case Action.WanderRight:
      this.speed.x = 2;
      var rightTiles = map.tilesOnLine(this.pos.x + this.width + this.speed.x, this.pos.y+1, this.pos.x + this.width + this.speed.x, this.pos.y + this.height-1);
      
      if(rightTiles.length > 0)
        this.action = Action.WanderLeft;
    break;
    }
    this.updatePosition(map);
  };
}

Enemy.prototype = new Actor();

Enemy.prototype.draw = function(offsetX, offsetY) {
  ctx.fillStyle = "rgb(250,0,0)";
	ctx.fillRect(this.pos.x - offsetX, this.pos.y - offsetY, this.height, this.width);
};

Enemy.prototype.onTouch = function(actor, map) {
  if(actor instanceof Box) {
    if(actor.speed.y > 19) {
      map.removeObject(this);
    }
  } else if(actor instanceof Hero) {
    actor.hp -= 1;
  }
};


/************************************
* Box
*************************************/
function Box(x, y) {
  this.pos = new Vector(x, y);
  this.height = 120;
  this.width  = 120;
  this.target = new Vector(0,0);
  this.followingMouse = false;
}

Box.prototype = new Actor();

Box.prototype.setTarget = function(x,y) {
  this.target.x = x;
  this.target.y = y;
  this.followingMouse = true;
};

Box.prototype.releaseTarget = function(x,y) {
  this.followingMouse = false;
}

Box.prototype.onTouch = function(hero, map) {

};

Box.prototype.enableGravity = function() { return !this.followingMouse; };

Box.prototype.draw = function(offsetX, offsetY) {
  ctx.fillStyle = "rgb(100,100,100)";
	ctx.fillRect(this.pos.x - offsetX, this.pos.y - offsetY, this.height, this.width);
	ctx.fillStyle = "rgb(200,200,200)";
	ctx.fillRect(this.pos.x - offsetX + 10, this.pos.y - offsetY + 10, this.height - 20, this.width - 20);
};

Box.prototype.update = function(map) {
  if(this.followingMouse) {
    this.speed = new Vector(this.target.x + map.offsetX, this.target.y + map.offsetY);
    this.speed.subtractVector(new Vector(this.pos.x + 60, this.pos.y + 60));
    if(this.speed.norm() < 10) {
      this.speed = new Vector(0,0);
    } else { 
      this.speed.normalize();
      this.speed.scalar(16);
    }
  }
  this.updatePosition(map);
};

/************************************
 * Hero 
 ************************************/
 
function Hero(x,y) {  
  this.width = 10; 
  this.height = 25;

  this.pos = new Vector(x, y);

  this.hp = 100;
  this.coins = 0;
  
  this.sheet = new Image();
  this.sheet.src = "img/hero.png";
  this.sheetOffsetX = 10;
  this.sheetOffsetY = 5;
}

Hero.prototype = new Actor();

Hero.prototype.jump = function() {
  this.onAir = true;
  this.speed.y = -15;
};

Hero.prototype.update = function(map) {
  // controls
  if(!this.onAir) {
    if(pressedKey[Key.LEFT_ARROW] && !pressedKey[Key.RIGHT_ARROW]) {
      this.changeAnimation(Animation.MovingLeft);
      this.speed.x = Math.max(this.speed.x - 1, -6);
    } else if(!pressedKey[Key.LEFT_ARROW] && pressedKey[Key.RIGHT_ARROW]) {
      this.changeAnimation(Animation.MovingRight);
      this.speed.x = Math.min(this.speed.x + 1, 6);
      }
  } else {
    if(pressedKey[Key.LEFT_ARROW] && !pressedKey[Key.RIGHT_ARROW]) {
      this.changeAnimation(Animation.JumpingLeft);
      this.speed.x = Math.max(this.speed.x - 0.3, -6);
    } else if(!pressedKey[Key.LEFT_ARROW] && pressedKey[Key.RIGHT_ARROW]) {
      this.changeAnimation(Animation.JumpingRight);
      this.speed.x = Math.min(this.speed.x + 0.3, 6);
    }
  }
  
  if(Math.abs(this.speed.x) < 0.2)
    this.stopAnimation();
  
  // TODO: wait til player releases jump key
  if(pressedKey[Key.UP_ARROW] && !this.onAir)
    this.jump();
  
  //
  this.updatePosition(map);
};
